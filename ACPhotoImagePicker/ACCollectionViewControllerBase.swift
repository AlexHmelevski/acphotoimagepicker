//
//  ACCollectionViewControllerBase.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit
import Photos

class ACCollectionViewControllerBase: UICollectionViewController, PeerCollectionViewController,ACPhotoSourceDelegate {
    var assetsSource = ACPhotoSourceFactory.getPhotoSourceFor(nil)
    var resource: PHObject = PHObject()
    var mediator: Mediator?
    var minimumCellSize: CGSize = CGSizeMake(90, 90)
    
    internal let reuseIdentifier = "Cell"
    required init(mediator: Mediator, collectionViewLayout: UICollectionViewLayout) {
        self.mediator = mediator
        super.init(collectionViewLayout: collectionViewLayout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let result = self.assetsSource.objectForIndex(indexPath) as? PHAssetCollection {
            let options = PHFetchOptions()
            options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            self.mediator?.didSelect(PHAsset.fetchAssetsInAssetCollection(result, options: options))
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {

        return self.assetsSource.numberOfSectionsInCollectionView(collectionView)
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assetsSource.collectionView(collectionView, numberOfItemsInSection: section)
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        fatalError("subclasses must override \(__FUNCTION__)")
          
    }
    
    func photoLibraryDidChange(inserted: [NSIndexPath], deleted: [NSIndexPath], changed: [NSIndexPath]) {
        fatalError("subclasses must override \(__FUNCTION__)")
        
    }
    
    
}
