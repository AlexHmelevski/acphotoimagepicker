//
//  ACImageStackThumbnail.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-27.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACImageStackThumbnail: UIView {
    var images = [UIImage]()
    
    init(images: [UIImage]) {
        super.init(frame: CGRectZero)
        self.images = images
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
