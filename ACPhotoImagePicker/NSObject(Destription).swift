//
//  NSObject+Kind.swift
//  MyWeatherApp
//
//  Created by Alex Crow on 15-11-20.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation
import UIKit
import Photos

extension NSObject {
    class func kind() -> String {
       return NSStringFromClass(self.classForCoder())
    }
}
