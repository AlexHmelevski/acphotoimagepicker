//
//  ACPhotoImagePicker.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-22.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit
import Photos

// MARK: - PROTOCOL SECTION

@objc public protocol ACPhotoImagePickerDelegate {
    func didSelectMedia(assets: Array<UIImage>)
}


public protocol Mediator {
    func didSelect(result: PHFetchResult)
    func didSelect(result: Array<UIImage>)
}

internal protocol PeerCollectionViewController {
    var assetsSource: ACPhotoSource {get set}
    var mediator: Mediator? {get set}
    init(mediator: Mediator, collectionViewLayout: UICollectionViewLayout)
}


// MARK: - CLASS SECTION

public class ACPhotoImagePicker: UINavigationController, Mediator {
    public var pickerDelegate: ACPhotoImagePickerDelegate?
    
    private let selectedObjects = [PHAsset]()
    
    public init() {
        super.init(navigationBarClass: nil, toolbarClass: nil)
        let button = UIBarButtonItem.init(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelButtonPressed")
        self.navigationItem.setRightBarButtonItem(button, animated: true)
        
    }
    
    override public func viewDidLoad() {
        
        PHPhotoLibrary.requestAuthorization({ (newStatus) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if newStatus == PHAuthorizationStatus.Authorized {
                    if let conrollerBuilder = ACCollectionViewControllerBuilder.getBuilder(self, fetchResult: nil, type: ACCollectionViewControllerBuilder.CollectionLayoutType.TABLE) {
                        let root = conrollerBuilder.build()
                        root.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem
                        self.viewControllers.append(root)
                    }
                    
                } else {
                    //TODO: Show alert
                  let alert = ACPhotoAlert.init()
                    alert.show(self, cancelComplition: { () -> Void in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    
                }
            })
        })

    }
    
    @nonobjc public  func  didSelect(result: PHFetchResult) {
        if result.count > 0 {
            if let conrollerBuilder = ACCollectionViewControllerBuilder.getBuilder(self, fetchResult: result, type: ACCollectionViewControllerBuilder.CollectionLayoutType.GRID) {
                let detail = conrollerBuilder.build()
                self.pushViewController(detail, animated: true)
                
            }
        }
    }

    @nonobjc public func didSelect(result: Array<UIImage>) {
        self.pickerDelegate?.didSelectMedia(result)
    }

    override public func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        for controller in self.viewControllers {
            if let colController = controller as? UICollectionViewController,
                let layout = colController.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.invalidateLayout()
            }
        }
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func cancelButtonPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
