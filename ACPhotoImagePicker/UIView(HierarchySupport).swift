//
//  UIView(HierarchySupport).swift
//  
//
//  Created by Alex Crow on 15-12-24.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
  // Return an array of all superviews »
    
    func superviews() -> Array<UIView> {
        var array: Array<UIView> = []
        var superview = self.superview
        repeat {
            if let view = superview {
                 array += [view]
                 superview = view.superview
            } else {
                break
            }
            
        } while true
        
        return array
    }

    // Return the nearest common ancestor between self and another view »
     func nearestCommonAncestor(forView view: UIView) -> UIView? {
     
        if self == view {return self}
        if self.isAncestor(ofView: view) {return self}
        if view.isAncestor(ofView: self) {return view}
        let ancestors = self.superviews()
        for aView in view.superviews() {
            if ancestors.contains(aView) {return aView}
        }
        
        return nil;
    }
    
    func addSubviews(subviews: UIView ...) {
        for view in subviews {
            self.addSubview(view)
            
        }
    }
    
    func setTranslatesAutoresizingMaskInSubviews(value: Bool) {
        for view in self.subviews {
            view.translatesAutoresizingMaskIntoConstraints = value;
        }
    }
    
    func constraintsReferenceView(view: UIView) -> Array<NSLayoutConstraint> {
        var array: Array<NSLayoutConstraint> = []
        for constraint in self.constraints {
            if constraint.firstItem.isEqual(view) || (constraint.secondItem != nil && constraint.secondItem!.isEqual(view)) {
                array += [constraint]
            }
        }
        
        return array
    }
    
    func containsObjectOfClass(classType: AnyClass) -> Bool {
        for subview in self.subviews {
            if subview.isKindOfClass(classType) {return true}
        }
        
        return false
    }
    
    private func isAncestor(ofView view: UIView) -> Bool {
        
        return view.superviews().contains(self)
    }
    
    
}



extension Float {
    var degreesToRadians: Float {
        return Float(self) * Float(M_PI) / 180
    }
}

extension UIImage {
    func imageRotatedByDegrees(degrees: Float) -> UIImage {
        let radians = CGFloat(degrees.degreesToRadians)
        
        let rotatedBox = UIView.init(frame: CGRectMake(0, 0, self.size.width, self.size.height))
        let t = CGAffineTransformMakeRotation(radians)
        rotatedBox.transform = t
        let rotatedBoxSize = rotatedBox.frame.size
        UIGraphicsBeginImageContextWithOptions(rotatedBoxSize, false, UIScreen.mainScreen().scale)
        let bitmap = UIGraphicsGetCurrentContext()
        
        CGContextTranslateCTM(bitmap, rotatedBoxSize.width / 2, rotatedBoxSize.height / 2)
        CGContextRotateCTM(bitmap, radians)
        
        CGContextScaleCTM(bitmap, 1.0, -1.0)
        CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), self.CGImage)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

    func normalizedImage() -> UIImage {
        if self.imageOrientation == UIImageOrientation.Up {
            return self
        } else {
            UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
            self.drawInRect(CGRect(origin: CGPointZero, size: self.size))
            let nImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return nImage
        }
    }
    
    
    class func stackOfImages(images: [UIImage], targetSize: CGSize) -> UIImage {
        
        let box = UIView.init(frame: CGRectMake(0, 0, targetSize.width, targetSize.height))
        
        for image in images {
            let imageView = UIImageView.init(image: image.imageRotatedByDegrees(Float(Int.random(-10...10))))
            imageView.frame = CGRectMake(2, 2, box.bounds.size.width, box.bounds.size.height)
            box.addSubview(imageView)
        }
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0)
        box.layer.renderInContext(UIGraphicsGetCurrentContext()!)
      
  
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}

extension Int
{
    static func random(range: Range<Int>) -> Int {
        var offset = 0
        
        if range.startIndex < 0  { // allow negative ranges
            offset = abs(range.startIndex)
        }
        
        let mini = UInt32(range.startIndex + offset)
        let maxi = UInt32(range.endIndex   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
}

