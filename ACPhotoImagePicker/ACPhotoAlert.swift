//
//  ACPhotoAlert.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACPhotoAlert {

    private var alertController: UIAlertController
    private var complition : (()-> Void)? = nil
    
    init() {
        self.alertController = UIAlertController.init(title: "Please Allow Photo Library Access", message: String(), preferredStyle: UIAlertControllerStyle.Alert)
        let goToSettings = UIAlertAction.init(title: "Open Settings", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
              UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        let dissmiss = UIAlertAction.init(title: "Don't Allow", style: UIAlertActionStyle.Cancel) { (action) -> Void in
            self.alertController.dismissViewControllerAnimated(true, completion: nil)
            if let compl = self.complition {
                compl()
            }
        }
        
        self.alertController.addAction(goToSettings)
        self.alertController.addAction(dissmiss)
       
    }
    
    func show(inController: UIViewController, cancelComplition: (()-> Void)?) {
        inController.presentViewController(self.alertController, animated: true, completion: nil)
        self.complition = cancelComplition
        
    }
    
    
}
