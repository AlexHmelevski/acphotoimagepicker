//
//  NSIndexPath(Ext).swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-02-01.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

extension NSIndexPath {
    class func createIndexPaths(fromSet nSet: NSIndexSet?) -> [NSIndexPath] {
        var array = [NSIndexPath]()
        if let set = nSet {
            set.enumerateIndexesUsingBlock { (idx, stop) -> Void in
                array.append(NSIndexPath(forItem: idx, inSection: 0))
            }
        }
        return array
    }
}
