//
//  PHFetchResultExtention.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import Photos

public func == (lhs: PHObject, rhs: PHObject) -> Bool {
    return lhs.localIdentifier == rhs.localIdentifier
}

extension PHFetchResult {
    
     func getAllAlbumsFrom(excludeEmpty excludeEmpty: Bool) -> [PHObject] {
        var anArray = [PHObject]()
        self.enumerateObjectsUsingBlock { (aObject, anIndex, stop) -> Void in
            if let aCollection = aObject as? PHAssetCollection {
                if excludeEmpty == true {
                    if (PHAsset.fetchAssetsInAssetCollection(aCollection, options: nil).count > 0) {
                        if !anArray.contains(aCollection) {
                            anArray.append(aCollection)
                        }
              
                    }
                } else {
                    anArray.append(aCollection)
                }

            } else if let anAsset = aObject as? PHAsset {
                anArray.append(anAsset)
            
            }
        }
        
        return anArray
    }
    
}

let queue = dispatch_queue_create("fetchQ", DISPATCH_QUEUE_SERIAL)

extension PHCachingImageManager {
    func getLatestImagesFrom(collection: PHAssetCollection, count: Int, size: CGSize, complition: (images: [UIImage]) -> Void) {

            let fOptions = PHFetchOptions()
            fOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            if #available(iOS 9.0, *) {
                fOptions.fetchLimit = count
            } else {
                // Fallback on earlier versions
            }
            let assetCollection = PHAsset.fetchAssetsInAssetCollection(collection, options: fOptions)
            var array = [UIImage]()
            
            let options = PHImageRequestOptions()
            options.synchronous = true
            options.resizeMode = PHImageRequestOptionsResizeMode.Exact
            options.normalizedCropRect = CGRectMake(0, 0, size.width , size.height)
            options.deliveryMode =  PHImageRequestOptionsDeliveryMode.HighQualityFormat
            for idx in  0...assetCollection.count - 1 {
                let asset = assetCollection.objectAtIndex(idx) as! PHAsset
                PHCachingImageManager.defaultManager().requestImageForAsset(asset, targetSize: size, contentMode: PHImageContentMode.AspectFill, options: options, resultHandler: { (image, info) -> Void in
                    if let nImage = image {
                        array.append(nImage)
                    }
                    if idx == assetCollection.count - 1 {
                        complition(images: array)
                        
                        
                    }
                    
                })
                
            }
        

        

    }
}


extension SequenceType where Generator.Element : PHObject {
    func unique() -> [Generator.Element] {
       return Array(Set(self))
    }
    
    func filterUnnessesary() -> [Generator.Element] {
        return self.filter({ (elem) -> Bool in
            if let cAsset = elem as? PHAssetCollection {
                
                return cAsset.assetCollectionSubtype != PHAssetCollectionSubtype.SmartAlbumTimelapses
                    && cAsset.assetCollectionSubtype != PHAssetCollectionSubtype.SmartAlbumVideos
                    && cAsset.assetCollectionSubtype != PHAssetCollectionSubtype.SmartAlbumRecentlyAdded
                    // 1000000201 the sign of recentlyDeleted Photos
                    && cAsset.assetCollectionSubtype.rawValue != 1000000201
            } else {
                
                return false
            }
        })
    }   
}

extension PHObject {
    override public var hashValue: Int {
        get {
            return self.localIdentifier.hashValue
        }
    }
}

extension PHAsset {
//    func getAssetURL() -> String {
//        
//        
//    }
//    
//    private func getExtention() -> String {
//        
//    }
//    
//    private func clearIdentifier ->String {
//        
//    }
    
}
