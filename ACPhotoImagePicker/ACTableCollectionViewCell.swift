//
//  ACTableCollectionViewCell.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACTableCollectionViewCell: UICollectionViewCell, PhotoCell {
    var image: ACImageAssetView?
    var title: UILabel = UILabel()
    var imageSize: CGSize {
        get {
            return self.contentView.bounds.size
        }
    }
    var representedIdentifier: String = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        self.image = ACImageAssetView.init(image: nil)
        super.init(frame: frame)
        self.contentView.addSubview(self.title)
        self.title.translatesAutoresizingMaskIntoConstraints = false
        self.title.textColor = UILabel.appearance().textColor
        self.title.numberOfLines = 0
        if let ovImage = self.image {
            self.contentView.addSubview(ovImage)
            ovImage.clipsToBounds = true
            ovImage.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.leading([ovImage, self.title], option: NSLayoutConstraint.VerticalAllignment.CENTER)
            NSLayoutConstraint.updateConstraintConstant(referenceView: self.title, value: 8, type: NSLayoutAttribute.Leading)
            NSLayoutConstraint.trailling([self.title], option: NSLayoutConstraint.VerticalAllignment.CENTER)
            NSLayoutConstraint.equalHeightToSuperView(views: [ self.title])
            NSLayoutConstraint.equalHeightToSuperView(views: [ovImage], multiplier: 0.9)
            NSLayoutConstraint.aspectRatio(width: 1.0, height: 1.0, views: ovImage)
            NSLayoutConstraint.updateConstraintConstant(referenceView: ovImage, value: 8, type: NSLayoutAttribute.Leading)
        }
    }
}
