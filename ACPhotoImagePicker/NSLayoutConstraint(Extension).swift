//
//  ACMWLayoutHelper.swift
// 
//
//  Created by Alex Crow on 15-12-22.
//  Copyright © 2015 Alex Crow. All rights reserved.
//

import UIKit

private protocol allignmentConvertion {
    var value: NSLayoutFormatOptions {get}
}

private extension  allignmentConvertion {
    func getAllignment() -> NSLayoutFormatOptions {
        return NSLayoutFormatOptions.AlignAllCenterY
    }

}
struct MetricsOption {
    var superviewTop: Double = 8
    var superviewBottom: Double = 8
    var superviewLeading: Double = 8
    var superviewTrailling: Double = 8
    var innerSpace: Double = 8
    init(top: Double, bottom: Double, leading: Double, trailing: Double, inner: Double) {
        superviewTop = top
        superviewBottom = bottom
        superviewTrailling = trailing
        superviewLeading = leading
        innerSpace = inner
    }
    
}

extension NSLayoutConstraint  {

    enum VerticalAllignment : allignmentConvertion  {
        case TOP
        case BOTTOM
        case CENTER
        var value: NSLayoutFormatOptions {
            var option: NSLayoutFormatOptions;
            switch(self) {
            case .TOP: option = NSLayoutFormatOptions.AlignAllTop
            case .BOTTOM: option = NSLayoutFormatOptions.AlignAllBottom
            case .CENTER: option = NSLayoutFormatOptions.AlignAllCenterY
            }
            
            return option
        }
      
    }
    
    enum HorizontalAllignment : allignmentConvertion {
        case LEADING
        case TRAILLING
        case CENTER
        var value: NSLayoutFormatOptions {
            var option: NSLayoutFormatOptions;
            switch(self) {
            case .LEADING: option = NSLayoutFormatOptions.AlignAllLeading
            case .TRAILLING: option = NSLayoutFormatOptions.AlignAllTrailing
            case .CENTER: option = NSLayoutFormatOptions.AlignAllCenterX
            }
            
            return option
        }
    }
    
    func install() -> Bool {
        if self.isUnary(),
           let firstView = self.firstItem as? UIView{
            firstView.addConstraint(self)
            
            return true
        }
        
        if let firstView = self.firstItem as? UIView,
            let secondView = self.secondItem as? UIView {
                if let view = firstView.nearestCommonAncestor(forView: secondView) {
                    view.addConstraint(self)
                    
                    return true
                }
        }
        print("ERROR: NO COMMON ACCESSORS between \(self.firstItem) and \(self.secondItem)")
        
        return false
    }
    
    func remove() -> Bool {
        if self.isUnary(),
            let firstView = self.firstItem as? UIView{
                firstView.removeConstraint(self)
                
                return true
        }
        
        if let firstView = self.firstItem as? UIView,
            let secondView = self.secondItem as? UIView {
                if let view = firstView.nearestCommonAncestor(forView: secondView) {
                    view.removeConstraint(self)
                    
                    return true
                }
        }
        print("ERROR: NO COMMON ACCESSORS between \(self.firstItem) and \(self.secondItem)")
        
        return false
    }
    
    class func allignVerticaly(views: Array<UIView>, horizontalAllignmentOptions option: HorizontalAllignment, boundToSuperView bound: Bool) {
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat(getFormat(FormatAxis.vertical, views: views, leadTop: bound, trailBottom: bound), options: option.value, metrics: nil, views: getBindings(views))
        installConstraints(constraints)
        turnOffAutoresizingMask(views)
    }

    class func trailling(views: Array<UIView>, option: VerticalAllignment) {
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat(getFormat(FormatAxis.horizontal, views: views, leadTop: false, trailBottom: true), options: option.value, metrics: nil, views: getBindings(views))
        installConstraints(constraints)
    }
    
    class func leading(views: Array<UIView>, option: VerticalAllignment) {
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat(getFormat(FormatAxis.horizontal, views: views, leadTop: true, trailBottom: false), options: option.value, metrics: nil, views: getBindings(views))
        installConstraints(constraints)
    }
    
    class func top(views: Array<UIView>, option: HorizontalAllignment) {
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat(getFormat(FormatAxis.vertical, views: views, leadTop: true, trailBottom: false), options: option.value, metrics: nil, views: getBindings(views))
        installConstraints(constraints)
    }
    
    class func bottom(views: Array<UIView>, option: HorizontalAllignment) {
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat(getFormat(FormatAxis.vertical, views: views, leadTop: false, trailBottom: true), options: option.value, metrics: nil, views: getBindings(views))
        installConstraints(constraints)
    }
    
    class func centerHorizontallyInSuperview(view: UIView) {
        if let superview = getSuperview([view]) {
            let constraint = NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: superview, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
            constraint.install()
        }
        turnOffAutoresizingMask([view])

    }
    
    class func allignHorizontaly(views: Array<UIView>, verticalAllignmentOptions option: VerticalAllignment, boundToSuperView bound: Bool) {
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat(getFormat(FormatAxis.horizontal, views: views, leadTop: bound, trailBottom: bound), options: option.value, metrics: nil, views: getBindings(views))
        for constraint in constraints {
            constraint.install()
        }
        turnOffAutoresizingMask(views)
    }
    
    class func equalWitdh(views views: Array<UIView>) {
        turnOffAutoresizingMask(views)
        installConstraints(getEqualSizeConstraints(.horizontal, views: views))
    }
    
    class func equalWitdh(views views: Array<UIView>, multiplier: Double) {
        if let superview = getSuperview(views) {
            for view in views {
                let constraint = NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: superview, attribute: NSLayoutAttribute.Width, multiplier: CGFloat(multiplier), constant: 0)
                constraint.install()
            }
        }
        turnOffAutoresizingMask(views)
    }
    
    class func compressionPriority(views: Array<UIView>, priority: Float) {
        for view in views {
            view.setContentCompressionResistancePriority(priority, forAxis: UILayoutConstraintAxis.Horizontal)
            view.setContentCompressionResistancePriority(priority, forAxis: UILayoutConstraintAxis.Vertical)
        }
    }
    
    class func hugging(views: Array<UIView>, priority: Float) {
        for view in views {
            view.setContentHuggingPriority(priority, forAxis: UILayoutConstraintAxis.Horizontal)
            view.setContentHuggingPriority(priority, forAxis: UILayoutConstraintAxis.Vertical)
        }
    }
    
    class func equalHeightToSuperView(views views: Array<UIView>) {
            installConstraints(getEqualSizeConstraints(.vertical, views: views))
            turnOffAutoresizingMask(views)
    }
    
    class func equalHeightToSuperView(views views: Array<UIView>, multiplier: Double) {
        if let superview = getSuperview(views) {
            for view in views {
                let constraint = NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: superview, attribute: NSLayoutAttribute.Height, multiplier: CGFloat(multiplier), constant: 0)
                constraint.install()
                turnOffAutoresizingMask(views)
            }
        }
    }
    
    class func aspectRatio(width width: Double, height: Double, views: UIView...) {
        for view in views {
            let constraint = NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Height, multiplier: CGFloat(width / height), constant: 0)
            constraint.install()
        }
    }
    
    class func updateConstraintConstant(referenceView view: UIView, value: Double, type: NSLayoutAttribute) {
        if let array = getConstraints(view, attribute: type) {
            for constraint in array {
                if constraint.secondItem!.isEqual(view) {
                   constraint.constant = CGFloat(value)
                } 
                    else if constraint .firstItem.isEqual(view) {
                    constraint.constant = CGFloat(value)
                }
            }
            
            if array.count == 0 {
                print("\(__FUNCTION__): There are no constraints for this requery ")
            }
        }
    }
    
    class  func changeConstraintRelation(referenceView view: UIView, relation: NSLayoutRelation, attribute: NSLayoutAttribute) {
        var constraintToRemove: NSLayoutConstraint?;
        if let array = getConstraints(view, attribute: attribute) {
            for constraint in array {
                if constraint.secondItem!.isEqual(view) {
                    constraintToRemove = constraint
                }
            }
        }
        if let constr = constraintToRemove {
            let newConstraint = NSLayoutConstraint.init(item: constr.firstItem, attribute: constr.firstAttribute, relatedBy: relation, toItem: constr.secondItem, attribute: constr.secondAttribute, multiplier: constr.multiplier, constant: constr.constant)
            constr.remove()
            newConstraint.install()
        }
    }
    
    
    private enum FormatAxis : String {
        case horizontal
        case vertical
        func value() -> String {
            switch (self) {
            case horizontal : return "H:"
            case vertical : return "V:"
            }
        }
    }
 
    private class func getEqualSizeConstraints(axisType: FormatAxis, views: Array<UIView>) -> Array<NSLayoutConstraint> {
       var format: String = axisType.value()
        format += "|"
        var constraints = [NSLayoutConstraint]()
        
        for(index, _) in views.enumerate(){
            constraints += NSLayoutConstraint.constraintsWithVisualFormat(format + "[view\(index)]|", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: getBindings(views))
        }
        
        return constraints
    }
    
    private class func getFormat(axisType: FormatAxis, views: Array<UIView>, leadTop: Bool, trailBottom: Bool ) -> String  {
        var format: String = axisType.value()
        if leadTop {
            format += "|"
        }


        for(index, _) in views.enumerate(){
            format += "[view\(index)]"
        }
        
        if trailBottom {
            format += "|"
        }
        
        return format
    }
    
    private class func getBindings(views: Array<UIView>) -> [String : UIView] {
        var bindings = [String : UIView]()

        for(index, view) in views.enumerate(){
            bindings["view\(index)"] = view
        }
        
        return bindings
    }
    
    private class func getSuperview(views: Array<UIView>) -> UIView? {
        var view: UIView
        if !views.isEmpty {
            for subview in views {
                if subview.superview == nil {
                    fatalError("no superview for view: \(views[0])")
                }
            }
            view = views[0]
            if let superview = view.superview {
                    return superview
            } else {
                return nil
            }
        }
        
        return nil
    }
    
    private class func turnOffAutoresizingMask(views: Array<UIView>) {
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false;
        }
    }
    
    private func isUnary() -> Bool {
        if self.secondItem == nil {return true}
        return false
    }
    
    private class func getConstraints(view: UIView, attribute: NSLayoutAttribute) -> Array<NSLayoutConstraint>? {
        return (view.superview?.constraintsReferenceView(view))?.filter({ (constraint) -> Bool in
            let firstValue = constraint.firstAttribute.rawValue
            let secondValue = constraint.secondAttribute.rawValue
            let secondView = constraint.secondItem as? UIView
            if firstValue == attribute.rawValue && constraint.firstItem.isEqual(view) ||
                secondView != nil && secondView!.isEqual(constraint.secondItem) && secondValue == attribute.rawValue{
                    return true
            } else {
                return false
            }
        })
    }
    
    private class func installConstraints(constraints: Array<NSLayoutConstraint>) {
        for constraint in constraints {
            constraint.install()
        }
     }
}
