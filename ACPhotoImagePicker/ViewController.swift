//
//  ViewController.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-29.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        self.presentViewController(ACPhotoImagePicker(), animated: true, completion: nil)
    }
}

