//
//  ACPhotoSource.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit
import Photos

protocol ACPhotoSource {
    var delegate: ACPhotoSourceDelegate? {get set}
    var assetCollection: [PHObject] {get set}
    var selectedObjects: [PHObject] {get set}
    
    init(assetSource: PHFetchResult?)
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    func configureCell(cell: PhotoCell, indexPath: NSIndexPath, section: Int)
    func objectForIndex(indexPath: NSIndexPath) -> PHObject?
    func selectObjectAt(object: PHObject)
    func deselectObject(object: PHObject)
    func getSelectedAssets() -> Array<UIImage>
}

protocol ACPhotoSourceDelegate {
    func photoLibraryDidChange(inserted: [NSIndexPath], deleted: [NSIndexPath], changed: [NSIndexPath])
}

private protocol ACPhotoSourcePrivate {
    var cashingManager: PHCachingImageManager {get set}
}


class ACPhotoSourceFactory {
    class func getPhotoSourceFor(fetchResult: PHFetchResult?) -> ACPhotoSource {
        if let result = fetchResult {
            return ACPhotoCollectionSource.init(assetSource: result)
        } else {
            return ACPhotoRootSource.init(assetSource: nil)
        }
    }
}

// MARK: - ACPhotoSourceBase

private class ACPhotoSourceBase: NSObject,  ACPhotoSource, ACPhotoSourcePrivate, PHPhotoLibraryChangeObserver {
    var cashingManager: PHCachingImageManager
    var assetCollection: [PHObject]  = [PHObject]()
    var delegate: ACPhotoSourceDelegate?
    var selectedObjects: [PHObject]  = [PHObject]()
    private var fetchResult: [PHFetchResult] = [PHFetchResult]()
    
    required init(assetSource: PHFetchResult?){
        self.cashingManager = PHCachingImageManager()
        super.init()
        if let fetch = assetSource {
            self.fetchResult.append(fetch)
        }

        self.convertFetchToArray()
        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
    }
    
    func convertFetchToArray() {
        // need to convert fetchResults into Array of PHObject.
         fatalError("subclasses must override \(__FUNCTION__)")
    }
    
    func configureCell(cell: PhotoCell, indexPath: NSIndexPath, section: Int) {
        fatalError("subclasses must override \(__FUNCTION__)")
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assetCollection.count
    }
    
    func selectObjectAt(object: PHObject) {
        if !self.selectedObjects.contains(object) {
            self.selectedObjects.append(object)
        }
    }
    
    func deselectObject(object: PHObject) {
        if let idx = selectedObjects.indexOf(object) {
            self.selectedObjects.removeAtIndex(idx)
        }
    }
    
    func getSelectedAssets() -> Array<UIImage> {
         fatalError("subclasses must override \(__FUNCTION__)")
    }
    
// MARK: - PHPhotoLibraryChangeObserver
    
    @objc func photoLibraryDidChange(changeInstance: PHChange) {
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            var deleted: NSIndexSet? = nil
            var inserted: NSIndexSet? = nil
            var updated: NSIndexSet? = nil
            var nFetchArray = [PHFetchResult]()
            
            for assets in self.fetchResult {
                if let changes =  changeInstance.changeDetailsForFetchResult(assets) {
                    
                    let nAssets = changes.fetchResultAfterChanges
                    nFetchArray.append(nAssets)
                    if self.fetchResult.count == 1 {
                        self.assetCollection = nAssets.getAllAlbumsFrom(excludeEmpty: true)
                    }

                    deleted = changes.removedIndexes
                    inserted = changes.insertedIndexes
                    updated = changes.changedIndexes
                }
             
            }
            
            if nFetchArray.count > 0 {
                self.fetchResult = nFetchArray
                self.delegate?.photoLibraryDidChange(NSIndexPath.createIndexPaths(fromSet: inserted),
                    deleted: NSIndexPath.createIndexPaths(fromSet: deleted),
                    changed: NSIndexPath.createIndexPaths(fromSet: updated))
            } else if self.fetchResult.count == 0 {
                    self.delegate?.photoLibraryDidChange(NSIndexPath.createIndexPaths(fromSet: inserted),
                        deleted: NSIndexPath.createIndexPaths(fromSet: deleted),
                        changed: NSIndexPath.createIndexPaths(fromSet: updated))
            }
        }
    }
    
    
    
    func objectForIndex(indexPath: NSIndexPath) -> PHObject? {
        return self.assetCollection[indexPath.row]
    }
    
    deinit {
        PHPhotoLibrary.sharedPhotoLibrary().unregisterChangeObserver(self)
    }
}

// MARK: - ROOT ACPhotoCollectionSource

private class ACPhotoRootSource: ACPhotoSourceBase {
    
    
    override func convertFetchToArray() {
        self.createAssetCollection()
    }

    override func configureCell(var cell: PhotoCell, indexPath: NSIndexPath, section: Int) {
        if let asset = self.assetCollection[indexPath.row] as? PHAssetCollection {
            let result = PHAsset.fetchAssetsInAssetCollection(asset, options: nil)
            cell.title.text = asset.localizedTitle! + String(" (\(result.count))")
            let scale = UIScreen.mainScreen().scale
            self.cashingManager.getLatestImagesFrom(asset, count: 3, size: CGSizeMake(cell.imageSize.height * scale, cell.imageSize.height * scale), complition: { (images) -> Void in
                cell.image?.setMainImage(images[0])
            })
        }
    }
    
    private func createAssetCollection() {
        let album = PHAssetCollection.fetchAssetCollectionsWithType(PHAssetCollectionType.Album, subtype: PHAssetCollectionSubtype.Any, options: nil)
        let topLevelCollection = PHCollectionList.fetchTopLevelUserCollectionsWithOptions(nil)
        let smart = PHAssetCollection.fetchAssetCollectionsWithType(PHAssetCollectionType.SmartAlbum, subtype: PHAssetCollectionSubtype.Any, options: nil)
        
        var array = album.getAllAlbumsFrom(excludeEmpty: true)
        array.appendContentsOf(topLevelCollection.getAllAlbumsFrom(excludeEmpty: true))
        array.appendContentsOf(smart.getAllAlbumsFrom(excludeEmpty: true))
        
        self.assetCollection = array.filterUnnessesary().unique().sort({ (obj1, obj2) -> Bool in
            if let col1 = obj1 as? PHAssetCollection,
                let col2 = obj2 as?    PHAssetCollection,
                let title1 = col1.localizedTitle,
                let title2 = col2.localizedTitle {
                    return title1 > title2
            } else {
                return false
            }
        })
    }
}


// MARK: - ACPhotoCollectionSource

private class ACPhotoCollectionSource: ACPhotoSourceBase {
    
    
    required init(assetSource: PHFetchResult?) {
        super.init(assetSource: assetSource)
        let array = self.assetCollection.map { (obj) -> PHAsset in
            return obj as! PHAsset
        }
       //TODO: Cashing properly and take target size outsize
        self.cashingManager.startCachingImagesForAssets(array, targetSize: CGSizeMake(90, 90), contentMode: PHImageContentMode.AspectFill, options: nil)
        self.cashingManager.stopCachingImagesForAssets(array, targetSize: CGSizeMake(90, 90), contentMode: PHImageContentMode.AspectFill, options: nil)
    }
 
    override func convertFetchToArray() {
        for array in self.fetchResult {
              self.assetCollection.appendContentsOf(array.getAllAlbumsFrom(excludeEmpty: true))
        }
    }
    
    override func configureCell(var cell: PhotoCell, indexPath: NSIndexPath, section: Int) {
            if let asset = self.assetCollection[indexPath.row] as? PHAsset {
                cell.representedIdentifier = asset.localIdentifier
                let scale = UIScreen.mainScreen().scale
                let options = PHImageRequestOptions()
                options.resizeMode = PHImageRequestOptionsResizeMode.Exact
                options.normalizedCropRect = CGRectMake(0, 0, 90, 90)
                options.deliveryMode =  PHImageRequestOptionsDeliveryMode.HighQualityFormat
                self.cashingManager.requestImageForAsset(asset, targetSize: CGSizeMake(cell.imageSize.width * scale, cell.imageSize.height * scale), contentMode: PHImageContentMode.AspectFill, options: options, resultHandler: { (newImage, info) -> Void in
                        if let image = newImage {
                            if cell.representedIdentifier == asset.localIdentifier {
                                cell.image?.setMainImage(image)
                            }
                        }
                })
            }
    }
        
    override func getSelectedAssets() -> Array<UIImage> {
        var infos =  [UIImage]()
        for obj in self.selectedObjects {
            if let asset = obj as? PHAsset {
                let options = PHImageRequestOptions()
                options.deliveryMode =  PHImageRequestOptionsDeliveryMode.HighQualityFormat
                options.synchronous = true
                self.cashingManager.requestImageForAsset(asset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.AspectFill, options: options, resultHandler: { (newImage, info) -> Void in
                    if let image = newImage {
                        infos.append(image)
                    }
                })

            }
        }
        return infos
    }
}
