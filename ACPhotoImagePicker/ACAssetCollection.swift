//
//  ACAssetCollection.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit
import Photos

class ACAssetCollection: PHAssetCollection {
    
   override var hashValue: Int {
        return self.localIdentifier.hashValue
    }
    
}
