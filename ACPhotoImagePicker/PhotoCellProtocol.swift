//
//  PhotoCellProtocol.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoCell {
    var image: ACImageAssetView? {get set}
    var title: UILabel {get set}
    var imageSize: CGSize {get}
    var representedIdentifier: String {get set}
}
