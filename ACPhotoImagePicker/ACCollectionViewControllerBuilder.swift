//
//  ACCollectionViewControllerBuilder.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-23.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import Foundation
import Photos

class ACCollectionViewControllerBuilder {
    enum CollectionLayoutType {
        case GRID
        case TABLE
    }
    private var mediator: Mediator
    private var source: ACPhotoSource
    private var fetchResult: PHFetchResult?
    private var collectionLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    private var controllerClass: ACCollectionViewControllerBase.Type
   // private var cellClass: UICollectionViewCell.Type
    private var screenSize: CGSize {
        get {
            return UIScreen.mainScreen().bounds.size
        }
    }
    
    internal required init(mediator: Mediator, fetchResult: PHFetchResult?) {
        self.mediator = mediator
        self.fetchResult = fetchResult
        self.source = ACPhotoSourceFactory.getPhotoSourceFor(self.fetchResult)
        
        self.collectionLayout.minimumInteritemSpacing = 5
        self.collectionLayout.minimumLineSpacing = 5
        self.collectionLayout.scrollDirection = UICollectionViewScrollDirection.Vertical
        self.collectionLayout.sectionInset = UIEdgeInsets.init(top: 1, left: 1, bottom: 1, right: 1)
        self.collectionLayout.headerReferenceSize = CGSizeZero
        self.collectionLayout.footerReferenceSize = CGSizeZero
        self.controllerClass = ACCollectionViewControllerBase.self
    }
    
    class func getBuilder(mediator: Mediator, fetchResult: PHFetchResult?, type: CollectionLayoutType) -> ACCollectionViewControllerBuilder? {
        var impl: ACCollectionViewControllerBuilder.Type?
        switch(type) {
            case .GRID : impl = ACGridCollectionViewControllerBuilder.self
            case .TABLE: impl = ACTableCollectionViewControllerBuilder.self
        }
        
        return impl?.init(mediator: mediator, fetchResult: fetchResult)
    }
        
    func build() -> ACCollectionViewControllerBase {

        let controller = self.controllerClass.init(mediator: mediator, collectionViewLayout: self.collectionLayout)
        controller.assetsSource = source
        controller.assetsSource.delegate = controller
        
        return controller
        
    }
}

private class ACGridCollectionViewControllerBuilder: ACCollectionViewControllerBuilder {
    required init(mediator: Mediator, fetchResult: PHFetchResult?) {
        super.init(mediator: mediator, fetchResult: fetchResult)
        self.collectionLayout.itemSize = CGSize.init(width: 80, height: 80)
        self.controllerClass = ACGridCollectionViewController.self
    }
    
    
    
}

private class ACTableCollectionViewControllerBuilder: ACCollectionViewControllerBuilder {
    required init(mediator: Mediator, fetchResult: PHFetchResult?) {
        super.init(mediator: mediator, fetchResult: fetchResult)
        let itemWidth = self.screenSize.width
    
        self.collectionLayout.itemSize = CGSize.init(width: itemWidth - self.collectionLayout.minimumInteritemSpacing * 2, height: 95)
        self.collectionLayout.invalidateLayout()
        self.controllerClass = ACTableCollectionViewController.self
    }
}
