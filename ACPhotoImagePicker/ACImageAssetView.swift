//
//  ACUIImageAssetView.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACImageAssetView: UIImageView {
    var overlayImageView: UIImageView?;
    var selected: Bool = false
    
    var selectedImage: Bool {
        get {
            return self.selected
        }
        set {
            self.selected = newValue
            self.overlayImageView?.hidden = !newValue
        }
    }
    
    func setMainImage(image: UIImage) {
        self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.createOverlay()
        
    }
    
    override init(image: UIImage?) {
        super.init(image: nil)
        self.createOverlay()
    }
    
    private func createOverlay() {
        if let bundle = NSBundle.init(identifier: "Alex.Crow.ACPhotoImagePicker"),
            let path = bundle.pathForResource("Overlay", ofType: "png"),
            let image = UIImage.init(contentsOfFile: path) {
            self.overlayImageView = UIImageView.init(image: image)
        } else {
            if let image = UIImage(named: "Overlay.png"){
                self.overlayImageView = UIImageView.init(image: image)
            }
        }
        
        if let overlay = self.overlayImageView {
            self.addSubview(overlay)
            overlay.contentMode = UIViewContentMode.ScaleAspectFill
            NSLayoutConstraint.equalHeightToSuperView(views: [overlay])
            NSLayoutConstraint.equalWitdh(views: [overlay])
            NSLayoutConstraint.centerHorizontallyInSuperview(overlay)
            overlay.hidden = true
        }
        self.contentMode = UIViewContentMode.ScaleAspectFill
    }
}
