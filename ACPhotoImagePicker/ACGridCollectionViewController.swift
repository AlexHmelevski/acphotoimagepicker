//
//  ACGridCollectionViewController.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACGridCollectionViewController: ACCollectionViewControllerBase {
    private var cachedItemSize: Float = 0
    private var doneButton: UIBarButtonItem?
    private var savedButton: UIBarButtonItem?
    private var selectedCells = Set<Int>()
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
       super.collectionView(collectionView, didSelectItemAtIndexPath: indexPath)
        
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? ACAssetViewCell {
            if let obj = self.assetsSource.objectForIndex(indexPath) {
                if !cell.selectedCell {
                    self.assetsSource.selectObjectAt(obj)
                    if self.navigationItem.rightBarButtonItem != self.doneButton {
                        self.navigationItem.setRightBarButtonItem(self.doneButton, animated: true)
                    }
                    selectedCells.insert(indexPath.row)
                    
                } else {
                    self.assetsSource.deselectObject(obj)
                    if self.assetsSource.selectedObjects.count == 0 {
                         self.navigationItem.rightBarButtonItem = nil
                    }
                    selectedCells.remove(indexPath.row)
                }
              //  self.doneButton?.title = String("Upload \(self.assetsSource.selectedObjects.count) images ")
                cell.selectAsset()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.registerClass(ACAssetViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.doneButton =  UIBarButtonItem.init(title: NSLocalizedString("Done", comment: String()), style: UIBarButtonItemStyle.Plain, target: self, action: "dismiss")
        self.title = NSLocalizedString("Pick Photos", comment: String())
        super.viewWillAppear(animated)
    }
    
    func dismiss() {
        self.mediator?.didSelect(self.assetsSource.getSelectedAssets())
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ACAssetViewCell
        
        // Configure the cell
        if self.selectedCells.contains(indexPath.row) {
            cell.selectAsset()
        }
        self.assetsSource.configureCell(cell, indexPath: indexPath, section: 0)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let l = collectionViewLayout as! UICollectionViewFlowLayout
        let globalWidth = Float(collectionView.bounds.width)
        let itemWidth = Float(l.itemSize.width)
        let itemSpacing = Float(l.minimumLineSpacing)
        let insets = Float(l.sectionInset.left + l.sectionInset.right)
        
        let nColumns = Int((globalWidth - insets - itemSpacing) / (itemWidth - itemSpacing))
        let recomWidth = CGFloat((globalWidth - insets - itemSpacing * Float(nColumns - 1)) / Float(nColumns)) 
        
        return  CGSizeMake(recomWidth, recomWidth)
    }
    
    override func photoLibraryDidChange(inserted: [NSIndexPath], deleted: [NSIndexPath], changed: [NSIndexPath]) {
            
            self.collectionView?.performBatchUpdates({ () -> Void in
                if inserted.count > 0 {
                    self.collectionView?.insertItemsAtIndexPaths(inserted)
                }
                
                if deleted.count > 0 {
                    self.collectionView?.deleteItemsAtIndexPaths(deleted)
                }
                
            
                
                }, completion: {  (completed) -> Void in
                    if changed.count > 0  && completed{
                        self.collectionView?.reloadItemsAtIndexPaths(changed)
                    }
                    
            })
        
    }
        
    
}
