//
//  ACAssetViewCell.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-25.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACAssetViewCell: UICollectionViewCell, PhotoCell {
    var image: ACImageAssetView?
    var selectedCell: Bool = false
    var title: UILabel = UILabel()
    var representedIdentifier: String = ""
    var imageSize: CGSize {
        get {
            return self.contentView.bounds.size
        }
    }
    
    func selectAsset() {
        self.selectedCell = !self.selectedCell
        self.image?.selectedImage = self.selectedCell
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        self.image = ACImageAssetView.init(image: nil)
        super.init(frame: frame)
        if let ovImage = self.image {
            self.contentView.addSubview(ovImage)
            ovImage.clipsToBounds = true
            ovImage.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.centerHorizontallyInSuperview(ovImage)
            NSLayoutConstraint.equalHeightToSuperView(views: [ovImage])
            NSLayoutConstraint.aspectRatio(width: 1.0, height: 1.0, views: ovImage)
            self.contentView.backgroundColor = UIColor.blackColor()
            self.backgroundColor = UIColor.blackColor()
            self.opaque = true
        }
    }
    
    override func prepareForReuse() {
        self.image?.selectedImage = false
    }
    
}
