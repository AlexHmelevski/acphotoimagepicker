//
//  ACTableCollectionViewController.swift
//  ACPhotoImagePicker
//
//  Created by Alex Crow on 16-01-26.
//  Copyright © 2016 Alex Crow. All rights reserved.
//

import UIKit

class ACTableCollectionViewController: ACCollectionViewControllerBase, UICollectionViewDelegateFlowLayout {
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ACTableCollectionViewCell
        
        // Configure the cell
        self.assetsSource.configureCell(cell, indexPath: indexPath, section: 0)
        
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.registerClass(ACTableCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        
        return  CGSizeMake(collectionView.bounds.width - layout.sectionInset.right - layout.sectionInset.left, layout.itemSize.height)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = NSLocalizedString("Pick an Album", comment: String())
        super.viewWillAppear(animated)
    }
    
    override func photoLibraryDidChange(inserted: [NSIndexPath], deleted: [NSIndexPath], changed: [NSIndexPath]) {
        self.collectionView?.reloadData()        
        
    }
}
